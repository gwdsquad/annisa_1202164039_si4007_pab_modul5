package com.example.annisamelyanta.annisa_1202164039_si4007_pab_modul5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CreateArticleActivity extends AppCompatActivity {

    EditText etTitle, etArticle, etAuthor;
    Button btnPost;
    String mTitle, mArticle, mAuthor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_create_article );


        etTitle = findViewById(R.id.et_title_create);
        etArticle = findViewById(R.id.et_desc_create);
        etAuthor = findViewById(R.id.et_author_create);
        btnPost = findViewById(R.id.btn_post_create);

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                postArticle();
            }
        });
    }

    private void postArticle() {
        mTitle = etTitle.getText().toString().trim();
        mArticle = etArticle.getText().toString().trim();
        mAuthor = etAuthor.getText().toString().trim();

        if (mTitle.isEmpty()){
            msg("Title kosong");
            return;
        }
        if (mArticle.isEmpty()){
            msg("Article kosong");
            return;
        }
        if (mAuthor.isEmpty()){
            msg("Author kosong");
            return;
        }

        saveToDb();

    }

    private void saveToDb() {
    }


    private void msg(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE d MMM `yy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
